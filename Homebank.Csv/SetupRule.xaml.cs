﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Homebank.Csv.Model;
using Homebank.Csv.Rules;

namespace Homebank.Csv
{
    /// <summary>
    /// Interaction logic for SetupRule.xaml
    /// </summary>
    public partial class SetupRule : UserControl
    {
        private Transaction selectedTransaction = null;
        private List<IMatches> activeMatchRules = new List<IMatches>();
        private Rule editRule = null;
        private MainWindow mainWindow;

        public SetupRule()
        {
            InitializeComponent();
            this.Loaded += SetupRule_Loaded;
        }

        public void SetupWindow(MainWindow window)
        {
            this.mainWindow = window;
        }

        private void SetupRule_Loaded(object sender, RoutedEventArgs e)
        {
            this.ComboBoxPayType.ItemsSource = Enum.GetValues(typeof(PaymentType));
            this.ComboBoxPayType.SelectedIndex = 0;
        }

        public void EditRule(Rule rule)
        {
            this.selectedTransaction = null;
            this.activeMatchRules.Clear();
            this.editRule = rule;

            if (rule != null)
            {
                foreach (var findParam in rule.FindParams)
                {
                    this.activeMatchRules.Add(findParam);
                }

                var setCategory = (SetCategory) rule.ModifyParams.FirstOrDefault(x => x is SetCategory);

                if (setCategory != null)
                {
                    this.ComboBoxModifyType.SelectedIndex = 0;
                    this.TextBoxSetCategory.Text = setCategory.Category;
                }

                var setPaymentType = (SetPaymentType) rule.ModifyParams.FirstOrDefault(x => x is SetPaymentType);

                if (setPaymentType != null)
                {
                    this.ComboBoxModifyType.SelectedIndex = 1;
                    this.ComboBoxPayType.Text = setPaymentType.SetType.ToString();
                }
            }

            this.ViewerTransaction.Visibility = Visibility.Collapsed;
            this.PanelTextFind.Visibility = Visibility.Collapsed;
            this.PanelSetModify.Visibility = Visibility.Visible;
            this.PanelExisting.Visibility = Visibility.Visible;
            this.BtnSaveRule.Visibility = Visibility.Visible;

            UpdateFindRules();
        }

        private void UpdateFindRules()
        {
            this.PanelExistingRules.Items.Clear();

            foreach (var activeMatchRule in this.activeMatchRules)
            {
                this.PanelExistingRules.Items.Add(activeMatchRule.Summary());
            }
        }

        public void SetItem(Transaction transaction)
        {
            this.selectedTransaction = transaction;
            this.activeMatchRules.Clear();
            UpdateFindRules();
            this.editRule = null;

            this.ViewerTransaction.Visibility = Visibility.Visible;
            this.PanelTextFind.Visibility = Visibility.Collapsed;
            this.PanelExisting.Visibility = Visibility.Collapsed;
            this.PanelSetModify.Visibility = Visibility.Collapsed;
            this.BtnSaveRule.Visibility = Visibility.Collapsed;

            this.pDate.Inlines.Clear();
            this.pPayment.Inlines.Clear();
            this.Amount.Inlines.Clear();
            this.pPayee.Inlines.Clear();
            this.pName.Inlines.Clear();
            this.pCategory.Inlines.Clear();
            this.pMemo.Inlines.Clear();
            this.pTags.Inlines.Clear();

            this.pDate.Inlines.Add(transaction.date.ToString("dd.MM.yyyy"));
            this.pPayment.Inlines.Add(transaction.payment.ToString());
            this.Amount.Inlines.Add(transaction.amount.ToString());
            this.pPayee.Inlines.Add(transaction.payee);
            this.pName.Inlines.Add(transaction.info);
            this.pCategory.Inlines.Add(transaction.category);
            this.pMemo.Inlines.Add(transaction.memo);
            this.pTags.Inlines.Add(transaction.tags);
        }

        private void BtnPayeeRule_OnClick(object sender, RoutedEventArgs e)
        {
            SetDefaults();

            this.LabelTextFind.Content = "Maksunsaaja";
            this.TextBoxFindTerm.Text = this.selectedTransaction?.payee ?? "";
        }

        private void BtnNameRule_OnClick(object sender, RoutedEventArgs e)
        {
            SetDefaults();

            this.LabelTextFind.Content = "Nimi";
            this.TextBoxFindTerm.Text = this.selectedTransaction?.info ?? "";
        }

        private void BtnMemoRule_OnClick(object sender, RoutedEventArgs e)
        {
            SetDefaults();

            this.LabelTextFind.Content = "Memo";
            this.TextBoxFindTerm.Text = this.selectedTransaction?.memo ?? "";
        }

        private void SetDefaults()
        {
            this.PanelTextFind.Visibility = Visibility.Visible;
            this.PanelExisting.Visibility = Visibility.Visible;
            this.PanelSetModify.Visibility = Visibility.Visible;
            this.BtnSaveRule.Visibility = Visibility.Visible;

            if (this.selectedTransaction != null)
            {
                if (this.ComboBoxMoneyFlow.SelectedIndex != 0)
                {
                    if (this.selectedTransaction.amount >0)
                    {
                        this.ComboBoxMoneyFlow.SelectedIndex = 1;
                    }
                    else if (this.selectedTransaction.amount < 0)
                    {
                        this.ComboBoxMoneyFlow.SelectedIndex = 2;
                    }
                }
            }
        }

        private void BtnHasCard_OnClick(object sender, RoutedEventArgs e)
        {
            SetDefaults();
            this.activeMatchRules.Add(new HasCardNumber());
            this.PanelExisting.Visibility = Visibility.Visible;
            this.PanelTextFind.Visibility = Visibility.Collapsed;
            UpdateFindRules();
        }

        private void BtnClose_OnClick(object sender, RoutedEventArgs e)
        {
            this.mainWindow.columnRules.Width = new GridLength(1, GridUnitType.Star);

            this.mainWindow.columnRule.Width = new GridLength(0);
            this.mainWindow.columnResult.Width = new GridLength(1, GridUnitType.Star);
            this.mainWindow.GridRules.SelectedIndex = -1;
            this.mainWindow.ResetSelectedTransaction();
        }

        private void BtnSaveRule_OnClick(object sender, RoutedEventArgs e)
        {
            
            if (this.editRule == null)
            {
                this.editRule = new Rule() { Priority = -1};
            }

            this.editRule.FindParams.Clear();

            foreach (var matchRule in this.activeMatchRules)
            {
                this.editRule.FindParams.Add(matchRule);
            }

            this.editRule.ModifyParams.Clear();

            if (this.ComboBoxModifyType.SelectedIndex == 0)
            {
                this.editRule.ModifyParams.Add(new SetCategory(this.TextBoxSetCategory.Text));
            }
            else if (this.ComboBoxModifyType.SelectedIndex == 1)
            {
                if (Enum.TryParse(this.ComboBoxPayType.Text, out PaymentType enumVal))
                {
                    this.editRule.ModifyParams.Add(new SetPaymentType(enumVal));
                }
            }

            if (this.editRule.Priority < 0)
            {
                this.editRule.Priority = 500;
                this.mainWindow.AddRule(this.editRule);
            }
            else
            {
                this.mainWindow.UpdateRules();
            }
        }

        private void BtnSetMatchRule_OnClick(object sender, RoutedEventArgs e)
        {

            var moneyFlow = (MoneyFlow) ComboBoxMoneyFlow.SelectedIndex;

            if (moneyFlow == MoneyFlow.None)
            {
                moneyFlow = MoneyFlow.Both;
            }

            if (string.IsNullOrWhiteSpace(this.TextBoxFindTerm.Text))
            {
                this.activeMatchRules.Add(new All() { Flow = moneyFlow });
            }
            else
            {
                TypeAndTextMatches rule;
                switch ((string)this.LabelTextFind.Content)
                {
                    case "Maksunsaaja":
                        rule = new PayeeMatches();
                        break;
                    case "Nimi":
                        rule = new InfoMatches();
                        break;
                    case "Memo":
                        rule = new MemoMatches();
                        break;
                    default:
                        throw new ArgumentException("LabelTextFind");
                }

                rule.IgnoreCase = true;
                rule.Contains = this.ComboBoxTextFindType.SelectedIndex == 0;
                rule.Flow = moneyFlow;
                rule.TextToFind = this.TextBoxFindTerm.Text;
                this.activeMatchRules.Add(rule);
            }

            UpdateFindRules();
        }

        private void ComboBoxModifyType_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = (ComboBox) sender;
            if (comboBox.SelectedIndex == 0)
            {
                this.TextBoxSetCategory.Visibility = Visibility.Visible;
                this.ComboBoxPayType.Visibility = Visibility.Collapsed;
            }
            else if (comboBox.SelectedIndex == 1)
            {
                this.TextBoxSetCategory.Visibility = Visibility.Collapsed;
                this.ComboBoxPayType.Visibility = Visibility.Visible;
            }
        }
    }
}