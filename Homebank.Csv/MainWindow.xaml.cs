﻿#region Usings

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Homebank.Csv.Model;
using Homebank.Csv.Rules;
using Microsoft.Win32;

#endregion

namespace Homebank.Csv
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region  Public Fields and Properties

        internal List<Transaction> OriginalTransactions = new List<Transaction>();
        internal List<Transaction> ModifiedTransactions;
        private RuleManager Manager = RuleManager.Get;
        private string lastFileNameUsed;
        private int? selectedTransactionIndex;
        private Brush tempBrush;

        #endregion

        #region Common

        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, EventArgs e)
        {
            this.SetupRule.SetupWindow(this);

            UpdateRules();
        }

        private void FileDrop_OnDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // Note that you can have more than one file.
                string[] files = (string[]) e.Data.GetData(DataFormats.FileDrop);

                // Assuming you have one file that you care about, pass it off to whatever
                // handling code you have defined.
                HandleFileOpen(files[0]);
            }
        }

        private void HandleFileOpen(string filePath)
        {
            this.selectedTransactionIndex = null;
            this.lastFileNameUsed = filePath;
            var line = "";
            System.IO.StreamReader file = new System.IO.StreamReader(filePath);

            this.OriginalTransactions.Clear();

            while ((line = file.ReadLine()) != null)
            {
                var splits = line.Split('\t');
                if (splits.Length >= 13)
                {
                    // Probably correct line
                    var transaction = new NordeaTransaction();
                    var index = -1;
                    transaction.Kirjauspäivä = splits[++index];
                    transaction.Arvopäivä = splits[++index];
                    transaction.Maksupäivä = splits[++index];
                    transaction.Määrä = splits[++index];
                    transaction.Saaja_Maksaja = splits[++index];
                    transaction.Tilinumero = splits[++index];
                    transaction.BIC = splits[++index];
                    transaction.Tapahtuma = splits[++index];
                    transaction.Viite = splits[++index];
                    transaction.MaksajanViite = splits[++index];
                    transaction.Viesti = splits[++index];
                    transaction.Kortinnumero = splits[++index];
                    transaction.Kuitti = splits[++index];

                    if (DateTime.TryParse(transaction.Kirjauspäivä, out var date)
                        && decimal.TryParse(transaction.Määrä, out var amount))
                    {
                        var convertedTransaction = new Transaction(transaction);
                        this.OriginalTransactions.Add(convertedTransaction);

                        convertedTransaction.amount = amount;
                        convertedTransaction.date = date;
                        convertedTransaction.payment = 0; //!string.IsNullOrWhiteSpace(transaction.Kortinnumero) ? 6 : 4;
                        convertedTransaction.payee = transaction.Saaja_Maksaja;
                        convertedTransaction.info = transaction.Viesti;
                        convertedTransaction.memo = transaction.Tapahtuma;
                        convertedTransaction.category = ""; // ConvertCategory(convertedTransaction.info, transaction.Saaja_Maksaja, amount);
                        convertedTransaction.tags = "csv import";
                    }
                }
            }

            UpdateResults();
        }

        private void UpdateResults()
        {
            this.ModifiedTransactions = this.Manager.ApplyRules(this.OriginalTransactions);

            var temp = this.selectedTransactionIndex;
            this.GridTransactions.Items.Clear();

            foreach (var transaction in this.ModifiedTransactions)
            {
                this.GridTransactions.Items.Add(transaction);
            }

            if (temp.HasValue)
            {
                this.GridTransactions.SelectedIndex = temp.Value;
            }

            this.TxtResult.Text = ToCsv(this.ModifiedTransactions);
            SelectTransaction();
        }

        private void GridTransactions_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var grid = ((DataGrid) sender);
            var transaction = grid.SelectedItem as Transaction;

            this.selectedTransactionIndex = this.ModifiedTransactions.IndexOf(transaction);
            SelectTransaction();
        }

        private void SelectTransaction()
        {
            if (this.selectedTransactionIndex > 0)
            {
                this.SetupRule.SetItem(this.ModifiedTransactions[this.selectedTransactionIndex.Value]);
                this.columnRules.Width = new GridLength(0);
                this.columnRule.Width = new GridLength(1, GridUnitType.Star);
                this.columnResult.Width = new GridLength(1, GridUnitType.Star);
            }
        }

        private string ToCsv(IEnumerable<Transaction> lines)
        {
            var sb = new StringBuilder();

            foreach (var transaction in lines)
            {
                AppendText(sb, transaction.date.ToString("dd.MM.yyyy"));
                sb.Append(((int) transaction.payment).ToString() + ";");
                AppendText(sb, transaction.info);
                AppendText(sb, transaction.payee);
                AppendText(sb, transaction.memo);
                sb.Append(transaction.amount.ToString(CultureInfo.InvariantCulture) + ";");
                AppendText(sb, transaction.category);
                sb.Append(transaction.tags + "\r\n");
            }

            return sb.ToString();
        }

        private void AppendText(StringBuilder sb, string text)
        {
            sb.Append(text + ";");
        }

        private void MainGrid_OnPreviewDragEnter(object sender, DragEventArgs e)
        {
            this.tempBrush = ((Panel) sender).Background;
            ((Panel) sender).Background = new SolidColorBrush(Color.FromRgb(225, 225, 225));
        }

        private void MainGrid_OnDragLeave(object sender, DragEventArgs e)
        {
            ((Panel) sender).Background = this.tempBrush;
        }

        #endregion

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            this.Manager.Save();
        }

        private void BtnSaveTransactions_OnClick(object sender, RoutedEventArgs e)
        {
            if (this.lastFileNameUsed == null)
            {
                return;
            }

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = Path.GetFileNameWithoutExtension(this.lastFileNameUsed);
            dlg.DefaultExt = ".csv"; // Default file extension
            dlg.Filter = "Comma-separated values (.csv)|*.csv"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                var filename = dlg.FileName;
                System.IO.File.WriteAllText(@filename, this.TxtResult.Text);
            }
        }

        private void GridRules_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var grid = ((DataGrid) sender);
            var ruleSummary = grid.SelectedItem as RuleSummary;

            if (ruleSummary != null)
            {
                this.SetupRule.EditRule(ruleSummary.Rule);
                this.columnRules.Width = new GridLength(1, GridUnitType.Star);
                this.columnRule.Width = new GridLength(1, GridUnitType.Star);
                this.columnResult.Width = new GridLength(0);
            }
        }

        public void AddRule(Rule rule)
        {
            if (rule.ModifyParams.Any(x => x is SetPaymentType))
            {
                this.Manager.RuleCategories[0].Rules.Add(rule);
            }
            else
            {
                this.Manager.RuleCategories[1].Rules.Add(rule);
            }

            UpdateRules();
        }

        public void UpdateRules()
        {
            var summaries = RuleSummary.GetSummaries(this.Manager.RuleCategories);

            this.GridRules.Items.Clear();

            foreach (var ruleSummary in summaries)
            {
                this.GridRules.Items.Add(ruleSummary);
            }

            UpdateResults();
            SelectTransaction();
        }

        public void ResetSelectedTransaction()
        {
            this.selectedTransactionIndex = null;
        }

        private void BtnNewRule_OnClick(object sender, RoutedEventArgs e)
        {
            this.selectedTransactionIndex = null;
            this.SetupRule.EditRule(null);
            this.columnRules.Width = new GridLength(1, GridUnitType.Star);
            this.columnRule.Width = new GridLength(1, GridUnitType.Star);
            this.columnResult.Width = new GridLength(0);
        }

        private void BtnDeleteRule_OnClick(object sender, RoutedEventArgs e)
        {
            var ruleSummary = this.GridRules.SelectedItem as RuleSummary;

            if (ruleSummary != null)
            {
                this.Manager.RemoveRule(ruleSummary.Rule);
            }

            UpdateRules();
        }

        private void PanelOpenNordea_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = ".txt"; // Default file extension
            openFileDialog.Filter = "Text files (*.txt)|*.txt"; // Filter files by extension

            if (openFileDialog.ShowDialog() == true)
            {
                HandleFileOpen(openFileDialog.FileName);
            }
        }
    }
}