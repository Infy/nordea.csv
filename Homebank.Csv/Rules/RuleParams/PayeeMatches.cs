﻿using System.Runtime.Serialization;
using Homebank.Csv.Model;

namespace Homebank.Csv.Rules
{
    [DataContract(Name = "PayeeMatches", Namespace = "http://www.homebank.com")]
    public class PayeeMatches : TypeAndTextMatches
    {
        protected override string GetSummaryDataTypeString()
        {
            return "maksunsaaja";
        }

        protected override string GetTransactionString(Transaction transaction)
        {
            return transaction.payee;
        }
    }
}