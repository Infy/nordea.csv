﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using Homebank.Csv.Model;

namespace Homebank.Csv.Rules
{
    [DataContract(Name = "TypeAndTextMatches", Namespace = "http://www.homebank.com")]
    public abstract class TypeAndTextMatches : IMatches
    {
        [DataMember()]
        public MoneyFlow Flow { get; set; } = MoneyFlow.Both;
        [DataMember()]
        public bool IgnoreCase { get; set; } = true;
        [DataMember()]
        public bool Contains { get; set; }
        [DataMember()]
        public string TextToFind { get; set; }

        public bool IsMatch(Transaction transaction)
        {
            switch (this.Flow)
            {
                case MoneyFlow.None:
                    return false;
                case MoneyFlow.Income:
                    if (transaction.amount < 0) { return false; }
                    break;
                case MoneyFlow.Expense:
                    if (transaction.amount > 0) { return false; }
                    break;
                case MoneyFlow.Both:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            var text = GetTransactionString(transaction);

            if (this.Contains)
            {
                return ContainsText(text, this.TextToFind);
            }
            else
            {
                return TextMatches(text, this.TextToFind);
            }
        }

        protected abstract string GetTransactionString(Transaction transaction);
        protected abstract string GetSummaryDataTypeString();

        private bool ContainsText(string text, string contain)
        {
            var ignore = this.IgnoreCase ? CompareOptions.OrdinalIgnoreCase : CompareOptions.Ordinal;

            return CultureInfo.InvariantCulture.CompareInfo.IndexOf(text, contain, ignore) >= 0;
        }

        private bool TextMatches(string text, string contain)
        {
            var ignore = this.IgnoreCase ? CompareOptions.OrdinalIgnoreCase : CompareOptions.Ordinal;

            return CultureInfo.InvariantCulture.CompareInfo.IndexOf(text, contain, ignore) == 0
                   && CultureInfo.InvariantCulture.CompareInfo.IndexOf(contain, text, ignore) == 0;
        }

        public string Summary()
        {
            string summaryText = null;

            switch (this.Flow)
            {
                case MoneyFlow.None:
                    return "Ei koskaan.";
                case MoneyFlow.Income:
                    summaryText = "tulo ";
                    break;
                case MoneyFlow.Expense:
                    summaryText = "meno ";
                    break;
                case MoneyFlow.Both:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            summaryText += GetSummaryDataTypeString();

            if (Contains)
            {
                summaryText += " sisältää: ";
            }
            else
            {
                summaryText += " on sama kuin: ";
            }

            summaryText += this.TextToFind;

            return summaryText.FirstCharToUpper();
        }
    }
}