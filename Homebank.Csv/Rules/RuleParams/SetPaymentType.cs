﻿using System.Runtime.Serialization;
using Homebank.Csv.Model;

namespace Homebank.Csv.Rules
{
    [DataContract(Name = "SetPaymentType", Namespace = "http://www.homebank.com")]
    public class SetPaymentType : IModify
    {
        

        [DataMember()]
        public PaymentType SetType { get; set; }

        public SetPaymentType(PaymentType setType)
        {
            this.SetType = setType;
        }
        
        public void Apply(Transaction transaction)
        {
            transaction.payment = this.SetType;
        }

        public string Summary()
        {
            return "Aseta maksutyypiksi " + this.SetType.ToString();
        }
    }
}