﻿using System.Runtime.Serialization;
using Homebank.Csv.Model;

namespace Homebank.Csv.Rules
{
    [DataContract(Name = "HasCardNumber", Namespace = "http://www.homebank.com")]
    public class HasCardNumber : IMatches
    {

        public bool IsMatch(Transaction transaction)
        {
            return !string.IsNullOrWhiteSpace(transaction.OriginalNordeaTransaction.Kortinnumero);
        }

        public string Summary()
        {
            return "Kortin numero on olemassa";
        }
    }
}