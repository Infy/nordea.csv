﻿using System.Runtime.Serialization;
using Homebank.Csv.Model;

namespace Homebank.Csv.Rules
{
    [DataContract(Name = "RuleCategory", Namespace = "http://www.homebank.com")]
    public class SetCategory : IModify
    {
        [DataMember]
        public string Category { get; set; }

        public SetCategory()
        {
        }

        public SetCategory(string category)
        {
            this.Category = category;
        }

        public void Apply(Transaction transaction)
        {
            transaction.category = this.Category;
        }

        public string Summary()
        {
            return "Aseta kategoriaksi " + this.Category.ToString();
        }
    }
}