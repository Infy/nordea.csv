﻿using System;
using System.Runtime.Serialization;
using Homebank.Csv.Model;

namespace Homebank.Csv.Rules
{
    [DataContract(Name = "All", Namespace = "http://www.homebank.com")]
    public class All : IMatches
    {
        [DataMember()]
        public MoneyFlow Flow { get; set; } = MoneyFlow.Both;

        public bool IsMatch(Transaction transaction)
        {
            switch (this.Flow)
            {
                case MoneyFlow.None:
                    return false;
                case MoneyFlow.Income:
                    if (transaction.amount < 0) { return false; }
                    break;
                case MoneyFlow.Expense:
                    if (transaction.amount > 0) { return false; }
                    break;
                case MoneyFlow.Both:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return true;
        }

        public string Summary()
        {
            switch (this.Flow)
            {
                case MoneyFlow.None:
                    return "Ei koskaan.";
                case MoneyFlow.Income:
                    return "Tulo";
                case MoneyFlow.Expense:
                    return "Meno";
                case MoneyFlow.Both:
                    return "Kaikki";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}