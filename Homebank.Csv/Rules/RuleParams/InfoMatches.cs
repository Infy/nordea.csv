﻿using System.Runtime.Serialization;
using Homebank.Csv.Model;

namespace Homebank.Csv.Rules
{
    [DataContract(Name = "InfoMatches", Namespace = "http://www.homebank.com")]
    public class InfoMatches : TypeAndTextMatches
    {
        protected override string GetSummaryDataTypeString()
        {
            return "nimi";
        }

        protected override string GetTransactionString(Transaction transaction)
        {
            return transaction.info;
        }
    }
}