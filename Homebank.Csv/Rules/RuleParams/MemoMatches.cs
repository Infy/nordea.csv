﻿using System.Runtime.Serialization;
using Homebank.Csv.Model;

namespace Homebank.Csv.Rules
{
    [DataContract(Name = "MemoMatches", Namespace = "http://www.homebank.com")]
    public class MemoMatches : TypeAndTextMatches
    {
        protected override string GetSummaryDataTypeString()
        {
            return "memo";
        }

        protected override string GetTransactionString(Transaction transaction)
        {
            return transaction.memo;
        }
    }
}