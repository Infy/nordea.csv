﻿using Homebank.Csv.Model;

namespace Homebank.Csv.Rules
{
    public interface IModify : IRule
    {
        void Apply(Transaction transaction);
    }
}