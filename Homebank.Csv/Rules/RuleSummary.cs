﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Homebank.Csv.Rules
{
    public class RuleSummary
    {
        public string Category { get; set; }
        public string MatchSummary { get; set; }
        public int Priority { get; set; }
        public string ModifySummary { get; set; }
        public Rule Rule { get; }

        public RuleSummary(string category, Rule rule)
        {
            this.Category = category;
            this.Priority = rule.Priority;
            this.MatchSummary = string.Join(", ", rule.FindParams.Select(x => x.Summary()));
            this.ModifySummary = string.Join(", ", rule.ModifyParams.Select(x => x.Summary()));
            this.Rule = rule;
        }

        public static List<RuleSummary> GetSummaries(List<RuleCategory> managerRuleCategories)
        {
            var list = new List<RuleSummary>();

            foreach (var ruleCategory in managerRuleCategories.OrderBy(x => x.OrderNumber))
            {
                foreach (var rule in ruleCategory.Rules.OrderBy(x => x.Priority))
                {
                    list.Add(new RuleSummary(ruleCategory.Name, rule));
                }
            }

            return list;
        }
    }
}