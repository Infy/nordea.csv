﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Homebank.Csv.Model;

namespace Homebank.Csv.Rules
{
    public class RuleManager
    {
        public List<RuleCategory> RuleCategories = new List<RuleCategory>();
        static Type[] knownTypes = new[] 
        {
            typeof(All),
            typeof(HasCardNumber), typeof(InfoMatches), typeof(PayeeMatches), typeof(MemoMatches),
            typeof(SetCategory), typeof(SetPaymentType)
        };

        private const string filename = "converter_rules.xml";

        // Singleton instance for this class
        private static volatile RuleManager instance;

        //Shared object for controlling the lock
        static readonly object sharedLock = new object();

        public static RuleManager Get
        {
            get
            {
                // Uses double-checked locking to check if the instance
                // has been created or not. Should provide the most
                // performance while also being thread-safe.
                if (instance == null)
                {
                    lock (sharedLock)
                    {
                        if (instance == null)
                        {
                            //Create the instance
                            instance = ReadFromFile();
                        }
                    }
                }
                return instance;
            }
        }

        private static RuleManager ReadFromFile()
        {
            if (File.Exists(filename))
            {
                return ReadXml(filename);
            }
            else
            {
                return CreateDefault();
            }
        }

        public static void WriteXmlFile(string filename, RuleManager instance)
        {
            DataContractSerializer ser = new DataContractSerializer(typeof(RuleManager), knownTypes);
            
            FileStream writer = new FileStream(filename, FileMode.Create);
            ser.WriteObject(writer, instance);
            writer.Close();
        }

        public static RuleManager ReadXml(string filename)
        {
            using (var outputStream = new FileStream(filename, FileMode.Open))
            {
                using (var reader = XmlDictionaryReader.CreateTextReader(outputStream, new XmlDictionaryReaderQuotas()))
                {
                    DataContractSerializer ser = new DataContractSerializer(typeof(RuleManager), knownTypes);
                    // Deserialize the data and read it from the instance.
                    return (RuleManager)ser.ReadObject(reader, true);
                }
            }
        }

        public void Save()
        {
            WriteXmlFile(filename, this);
        }

        private static RuleManager CreateDefault()
        {
            var manager = new RuleManager();
            var categoryPaymentType = new RuleCategory()
            {
                Name = "Maksutyyppi",
                OrderNumber = 0
            };

            categoryPaymentType.Rules = new List<Rule>();
            categoryPaymentType.Rules.Add(new Rule(1002, new All(), new SetPaymentType(PaymentType.Transfer)));
            categoryPaymentType.Rules.Add(new Rule(1001, new MemoMatches() { TextToFind = "e-lasku"}, new SetPaymentType(PaymentType.ElectronicPayment)));
            categoryPaymentType.Rules.Add(new Rule(1000, new HasCardNumber(), new SetPaymentType(PaymentType.DebitCard)));

            var category = new RuleCategory()
            {
                Name = "Categoria",
                OrderNumber = 1
            };

            category.Rules = new List<Rule>();
            category.Rules.Add(new Rule(1002, new All() {Flow = MoneyFlow.Income}, new SetCategory("Tulot")));
            category.Rules.Add(new Rule(1001, new All() {Flow = MoneyFlow.Expense}, new SetCategory("Menot")));
            //category.Rules.Add(new Rule(1000, new PayeeMatches() {Flow = MoneyFlow.Expense, TextToFind = "steam", Contains = true}, new SetCategory("Pelit")));

            manager.RuleCategories.Add(categoryPaymentType);
            manager.RuleCategories.Add(category);
            return manager;
        }


        public List<Transaction> ApplyRules(List<Transaction> originalTransactions)
        {
            var list = new List<Transaction>(originalTransactions.Count);

            foreach (var originalTransaction in originalTransactions)
            {
                var convertedTransaction = new Transaction(originalTransaction.OriginalNordeaTransaction);
                list.Add(convertedTransaction);

                originalTransaction.CopyTo(convertedTransaction);

                ApplyRules(convertedTransaction);
            }

            return list;
        }

        private void ApplyRules(Transaction convertedTransaction)
        {
            foreach (var ruleCategory in this.RuleCategories.OrderBy(x => x.OrderNumber))
            {
                foreach (var rule in ruleCategory.Rules.OrderBy(x => x.Priority))
                {
                    if (rule.IsMatch(convertedTransaction))
                    {
                        // Apply only one rule per category
                        rule.Apply(convertedTransaction);
                        break;
                    }
                }
            }
        }

        public void RemoveRule(Rule rule)
        {
            foreach (var ruleCategory in this.RuleCategories)
            {
                for (int i = ruleCategory.Rules.Count - 1; i >= 0; i--)
                {
                    if (rule == ruleCategory.Rules[i])
                    {
                        ruleCategory.Rules.RemoveAt(i);
                    }
                }
            }
        }
    }
}
