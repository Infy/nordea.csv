﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Homebank.Csv.Rules
{
    [DataContract(Name = "RuleCategory", Namespace = "http://www.homebank.com")]
    public class RuleCategory
    {
        [DataMember()]
        public string Name { get; set; }
        [DataMember()]
        public int OrderNumber { get; set; }

        [DataMember()]
        public List<Rule> Rules { get; set; }
    }
}