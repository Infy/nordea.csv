﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Homebank.Csv.Model;

namespace Homebank.Csv.Rules
{
    public class Rule
    {

        /// <summary>
        /// Lower value is before higher value in order.
        /// </summary>
        [DataMember()]
        public int Priority { get; set; }

        [DataMember()]
        public List<IMatches> FindParams { get; set; } = new List<IMatches>();
        [DataMember()]
        public List<IModify> ModifyParams { get; set; } = new List<IModify>();

        public Rule(int priority, IMatches findParam, IModify applyParam)
        {
            this.Priority = priority;
            this.FindParams.Add(findParam);
            this.ModifyParams.Add(applyParam);
        }

        public Rule()
        {
        }

        public bool IsMatch(Transaction transaction)
        {
            if (this.FindParams?.Count > 0)
            {
                foreach (var match in this.FindParams)
                {
                    if (match.IsMatch(transaction))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public void Apply(Transaction transaction)
        {
            foreach (var modifyParam in this.ModifyParams)
            {
                modifyParam.Apply(transaction);
            }
        }
    }
}