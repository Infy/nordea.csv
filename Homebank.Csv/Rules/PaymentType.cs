﻿namespace Homebank.Csv.Rules
{
    public enum PaymentType
    {
        CreditCard = 1,
        Check = 2,
        Cash = 3,
        Transfer = 4,
        DebitCard = 6,
        StandingOrder = 7,
        ElectronicPayment = 8,
        Deposit = 9,
        FinancialInstitutionFee = 10
    }
}