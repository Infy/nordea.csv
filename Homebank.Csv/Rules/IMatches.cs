﻿using Homebank.Csv.Model;

namespace Homebank.Csv.Rules
{
    public interface IRule
    {
        string Summary();
    }

    public interface IMatches: IRule
    {
        bool IsMatch(Transaction transaction);
    }
}