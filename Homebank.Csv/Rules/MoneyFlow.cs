﻿namespace Homebank.Csv.Rules
{
    public enum MoneyFlow
    {
        None = 0,
        Income = 1,
        Expense = 2,
        Both = 3
    }
}