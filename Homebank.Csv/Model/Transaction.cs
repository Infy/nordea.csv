﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebank.Csv.Rules;

namespace Homebank.Csv.Model
{
    public class Transaction
    {
        public DateTime date { get; set; }
        public string DateText => this.date.ToString("dd.MM.yyyy");

        public PaymentType payment { get; set; }
        public string info { get; set; }
        public string payee { get; set; }
        public string memo { get; set; }
        public decimal amount { get; set; }
        public string category { get; set; }
        public string tags { get; set; }

        public readonly NordeaTransaction OriginalNordeaTransaction;

        public Transaction(NordeaTransaction originalNordeaTransaction)
        {
            this.OriginalNordeaTransaction = originalNordeaTransaction;
        }

        public override string ToString()
        {
            return this.DateText
                   + " " + this.payment
                   + " " + this.info
                   + " " + this.payee
                   + " " + this.memo
                   + " " + this.category
                   + " " + this.tags
                ;
        }

        public void CopyTo(Transaction otherTransaction)
        {
            otherTransaction.date = this.date;
            otherTransaction.payment = this.payment;
            otherTransaction.info = this.info;
            otherTransaction.payee = this.payee;
            otherTransaction.memo = this.memo;
            otherTransaction.amount = this.amount;
            otherTransaction.category = this.category;
            otherTransaction.tags = this.tags;
        }
    }
}