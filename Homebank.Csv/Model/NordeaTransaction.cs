﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homebank.Csv.Model
{
    public class NordeaTransaction
    {
        public string Kirjauspäivä { get; set; }
        public string Arvopäivä { get; set; }
        public string Maksupäivä { get; set; }
        public string Määrä { get; set; }
        public string Saaja_Maksaja { get; set; }
        public string Tilinumero { get; set; }
        public string BIC { get; set; }
        public string Tapahtuma { get; set; }
        public string Viite { get; set; }
        public string MaksajanViite { get; set; }
        public string Viesti { get; set; }
        public string Kortinnumero { get; set; }
        public string Kuitti { get; set; }
    }
}
